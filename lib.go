package i18n

import (
	"encoding/json"
	"fmt"
	"io"
	"io/fs"
)

// Translator loads messages into a struct of your choosing.
//
// Text domain groups messages. There should be one text domain per message
// struct type.
type Translator func(textDomain string, messages any) error

// NewTranslator creates a translator loading messages from the catalog with the
// given language code.
func NewTranslator(languageCode string, catalog Catalog) Translator {
	return func(textDomain string, messages any) error {
		return catalog.LoadMessages(languageCode, textDomain, &messages)
	}
}

// Catalog is the work horse doing the actual message loading.
type Catalog interface {
	LoadMessages(languageCode string, textDomain string, messages any) error
}

// FilesystemCatalog loads messages from JSON files.
//
// File names must be: <textDomain>.<languageCode>.json. Nested directories are
// not supported.
type FilesystemCatalog struct {
	files             fs.FS
	messagesDirectory string
}

// NewFilesystemCatalog.
//
// Message files are located in messages directory.
func NewFilesystemCatalog(files fs.FS, messagesDirectory string) Catalog {
	return &FilesystemCatalog{files, messagesDirectory}
}

func (f *FilesystemCatalog) LoadMessages(
	languageCode string,
	textDomain string,
	messages any,
) error {
	filename := fmt.Sprintf(
		"%s/%s.%s.json",
		f.messagesDirectory,
		textDomain,
		languageCode,
	)

	file, err := f.files.Open(filename)
	if err != nil {
		return errorAtFile(filename, err)
	}
	defer file.Close()

	contents, err := io.ReadAll(file)
	if err != nil {
		return errorAtFile(filename, err)
	}

	err = json.Unmarshal(contents, &messages)
	if err != nil {
		return errorAtFile(filename, err)
	}
	return nil
}

func errorAtFile(filename string, err error) error {
	return fmt.Errorf("message file %s: %w", filename, err)
}

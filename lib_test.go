package i18n

import (
	"embed"
	"testing"

	"github.com/stretchr/testify/require"
)

//go:embed translations
var testTranslations embed.FS

type flatMessages struct {
	Foo string `json:"foo"`
	Bar string `json:"bar"`
}

type nestedMessages struct {
	Foo struct {
		Bar struct {
			Quux string `json:"quux"`
		} `json:"bar"`
	} `json:"foo"`
}

func TestFlatTranslation(t *testing.T) {
	must := require.New(t)
	catalog := NewFilesystemCatalog(testTranslations, "translations")

	english := NewTranslator("en", catalog)
	var messages flatMessages
	err := english("flat", &messages)
	must.Nil(err)
	must.Equal("The Foo", messages.Foo)
	must.Equal("Bar fight, go!", messages.Bar)

	deutsch := NewTranslator("de", catalog)
	err = deutsch("flat", &messages)
	must.Nil(err)
	must.Equal("Das Foo", messages.Foo)
	must.Equal("Kneipenschlägerei, los!", messages.Bar)
}

func TestNestedTranslation(t *testing.T) {
	must := require.New(t)
	catalog := NewFilesystemCatalog(testTranslations, "translations")

	english := NewTranslator("en", catalog)
	var messages nestedMessages
	err := english("nested", &messages)
	must.Nil(err)
	must.Equal(
		"Q-uux gets rid of even the nastiest stain.",
		messages.Foo.Bar.Quux,
	)

	deutsch := NewTranslator("de", catalog)
	err = deutsch("nested", &messages)
	must.Nil(err)
	must.Equal(
		"Kuh-uux radiert selbst den übelsten Fleck aus.",
		messages.Foo.Bar.Quux,
	)
}

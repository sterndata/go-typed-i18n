# go-typed-i18n

Internationalization for Go programs leveraging the type system.

## Comparison to other libraries

When looking around for Go i18n libraries, I found two popular libraries:

* `github.com/nicksnyder/go-i18n`
  * workflow styled after gettext, complete with message
    extractors and message file updaters (mergers).
  * verbose API
  * plain (English) language strings in source code

* `github.com/kataras/i18n`
  * stringly typed message IDs
  * no workflow defined

## TODO

(when the need arises)

* extractor and merger program
* plural support
